#!/usr/bin/env ruby

require "benchmark"
require "tempfile"

require "arrow"
require "json"
require "csv"

tmp_dir = "/dev/shm"
unless File.directory?(tmp_dir)
  tmp_dir = nil
end

def serialize_json(numbers, tmp_dir)
  file = Tempfile.new(["bench-serialize", ".json"], tmp_dir)
  file.open
  JSON.dump(numbers, file)
  file.close
  file
end

def deserialize_json(file)
  file.open
  JSON.load(file)
  file.close
end

def serialize_arrow(numbers, tmp_dir)
  file = Tempfile.new(["bench-serialize", ".arrow"], tmp_dir)
  numbers.save(file.path)
  file
end

def deserialize_arrow(file)
  Arrow::Table.load(file.path)
end

def serialize_csv(numbers, tmp_dir)
  file = Tempfile.new(["bench-serialize", ".csv"], tmp_dir)
  file.open
  CSV.open(file.path, "w") do |csv|
    numbers.each do |number|
      csv << number
    end
  end
  file
end

def deserialize_csv(file)
  CSV.read(file.path)
end

Benchmark.bmbm do |job|
  [
    "JSON",
    "Arrow",
    "CSV",
  ].each do |format|
    [
      10000,
      100000,
      1000000,
    ].each do |n|
      srand(0)
      numbers = n.times.collect {rand}
      case format
      when "Arrow"
        numbers = Arrow::Table.new("number" => Arrow::Int32Array.new(numbers))
      when "CSV"
        numbers = numbers.collect do |number|
          [number]
        end
      end
      job.report("#{format}: serialize: #{n}") do
        __send__("serialize_#{format.downcase}",
                 numbers,
                 tmp_dir)
      end

      file = __send__("serialize_#{format.downcase}",
                      numbers,
                      tmp_dir)
      job.report("#{format}: deserialize: #{n}") do
        __send__("deserialize_#{format.downcase}",
                 file)
      end
    end
  end
end
